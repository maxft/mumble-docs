# Mumble Handbook

This is a handbook for Mumble voice chat. It uses mdBook for content management and serving.

# Viewing

To run the documentation locally, make sure you have the rust toolchain installed. Then, run the following commands:

1. `cargo install mdbook`
2. `cargo install mdbook-theme`
3. `mdbook build`
4. `mdbook serve`

Now, go to your web browser and go to http://localhost:3000 to view the handbook.
