# Utilities 

This section discusses the user utilities Mumble provides.

## Server Connect

The server connect utility displays the public server list and your favorited servers. From there, you can connect to servers:

![server-connect](images/server-connect.png)

Click the globe icon in the toolbar to access it:

![server-icon](images/server-icon.png)

To add a server, Click *Add New...*. Then, enter in the requested information:

![add-new](images/add-new.png)

## Server Information

The server information utility displays information about your connection to a server:

![server-information](images/server-information.png)

Click the 'i' icon in the toolbar to access it:

![information-icon](images/information-icon.png)

Clicking on *View Certificate* shows the SSL certificate the server uses:

![cert](images/cert.png)

## Recorder

The recorder utility records audio in the channel you're currently in:

![recorder](images/recorder.png)

Click the red circle icon in the toolbar to access it:

![recorder-icon](images/recorder-icon.png)

There are four file formats the recorder can use:

* **.ogg** - Compressed lossy and uses Opus
* **.wav** - Raw lossless audio
* **.au** - Another lossless format
* **.flac** - Compressed lossless audio

The table below describes the recorder's modes:

| **Mode** | **Description** |
| -------- | --------------- |
| Downmix | Records all channel audio into a single file. Takes less space |
| Multichannel | Records audio for each user in a dedicated file. Good for podcasts |

<br>For the privacy of other users, Mumble sends a message in the client logs of all users when a recording starts. Mumble also shows a recording icon next to the user while they record.</br>

## Channel Filter

The channel filter removes channels without users from view. This is useful on servers with many channels. Click the green funnel icon in the toolbar to access it:

![channel-filter](images/channel-filter.png)

## Search

The search utility lets you search for users and channels on a server. Clicking on the gear icon to the right lets you customize search:

![search](images/search.png)

Enabling Case-sensitive makes Mumble distinguish between uppercase and lowercase letters. Enabling RegEx makes Mumble interpret the entered text as a regular expression.

## Change Comment

Users can set comments that appear when you hover over their name. To change your comment, click the purple message box in the toolbar:

![comment](images/comment.png)

## Change Avatar

Users can set a profile picture (avatar) that appears when you hover over their name. To set your avatar, click *Self* in the menu bar and select *Change Avatar...*.

![change-avatar](images/change-avatar.png)

To delete your avatar, click *Self* in the menu bar and select *Remove Avatar*.

![remove-avatar](images/remove-avatar.png)

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Mumble sets comments and avatars on a per-server basis rather than globally.</a>
> </div>

## Talking UI

To enable the Talking UI, go to *Configure* in the menu bar and click *Talking UI*

![talking-ui](images/talking-ui.png)

You can learn how to configure Talking UI [here](https://maxft.gitlab.io/mumble-docs/chapter_4/settings/user_interface.html#talking-ui)

## Channel Listeners

Channel Listeners allow you to listen to the audio in channels you're not in. To listen to a channel's audio, right click it and click *Listen to channel*:

![channel-listener](images/channel-listener.png)

You may listen to multiple channels at once. For privacy, Mumble shows your listeners to all users with an ear icon. It looks like this:

![ears](images/ears.png)

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Channel listeners are persistent; mumble remembers them after disconnecting and activates them when you join again.</a>
> </div>

## Channel Linking

Channel linking allows audio transmission across multiple channels automatically. Use the *Link* action in the channel context menu to specify these channels. Here's how to link channels to each other:

1. Join the channel you want to link from
2. Right-click the first channel you want to link to and select *Link*
3. Repeat this with other channels you want to link to

## Mumble URL

Mumble has its own Uniform Resource Identifier (URI) scheme. A Uniform Resource Identifier specifies the protocol or application that opens a link. Mumble URIs have the following format:

`mumble://[username[:password]@]<address>[:port]/[channelpath]?version=<serverversion>[&title=<servername>][&url=<serverurl>]`

If you want a user to join Mumble in a specified channel, right-click the desired channel and select Copy URL. Then, send them the link and tell them to enter it in their browser. Mumble will open and join the user in the desired channel.

## Minimal View

Minimal View hides the menu bar and toolbar. You can enable it by clicking *Configure* in the menu bar and selecting *Minimal View*. To unset minimal view, right-click the channel view pane, click
*Configure*, then select *Minimal View*:

![minimal-view](images/minimal-view.png)
