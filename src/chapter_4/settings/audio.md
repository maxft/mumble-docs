# Audio

Mumble launches the Audio Wizard on its first start. The Audio Wizard prompts you to configure audio settings. The settings interface lets you change your initial choices in the Audio Wizard and access all audio settings. Mumble splits audio settings into two categories:

* Audio Input
* Audio Output

## Input

Input settings control your microphone. There are five settings:

### Interface

![interface](images/interface.png)

*System* selects the sound system that Mumble uses. Windows and MacOS users likely won't need to change this setting. *Device* selects the microphone that Mumble uses.

### Transmission

Transmission controls how your microphone transmits audio. The *Transmit* setting has three options:

* Push to talk
* Voice Activity
* Continuous

Push to talk and Voice Activity have extended settings, while Continuous doesn't.

<h4>Push to talk</h4>

Push-to-talk transmits audio when you push the push-to-talk key. You can set this key in *Shortcuts*. 

![push-to-talk](images/push-to-talk.png)

The table below describes the three options associated with the Push to talk setting:

| **Options** | **Description** |
| ----------- | --------------- |
| DoublePush Time | If you press the push-to-talk button twice in this period, Mumble locks push-to-talk on (measured in milliseconds) |
| Hold Time | The amount of time the microphone stays on after you release the push-to-talk key (measured in milliseconds) |
| Display Push to Talk Window | Displays a push-to-talk button that covers the main Mumble window |

<h4>Voice Activity</h4>

Voice Activity transmits all audio over the yellow bar. The *Voice Hold* setting sets the amount of time (in milliseconds) that audio transmission shall continue after silence.

![voice-activity](images/voice-activity.png)

You can adjust the starting and ending thresholds of the yellow bar with the *Silence Below* and *Silence Above* options. The table below describes Mumble's two speech recognition options and the corresponding scenarios for each:

| **Option** | **Description** | **Scenario** |
| ---------- | --------------- | ------------ |
| Amplitude | Captures the raw microphone input | Cardioid (directional) microphones |
| Signal to Noise | Distinguishes between your voice and background noise and attempts to filter out the noise | Omnidirectional microphones |

### Compression

Compression controls the bandwidth of the audio input. Quality sets the bandwidth limit (in kilobits-per-second), and Audio Per Packet sets the number of audio frames per network packet. Allow low delay mode decreases the latency of Opus when Quality is at least 64 kilobits-per-second. The figure below shows the quality trade-off that the user faces:

![compression](images/compression.png)

### Audio Processing

Audio Processing controls audio input amplification, echo cancellation, and noise suppression. *Max. Amplification* sets the factor Mumble increases the audio input by. *Echo Cancellation* filters echo from microphone inputs by using Speex. The table below describes the three options for *Echo Cancellation* and the corresponding scenarios for each:

| **Option** | **Description** | **Scenario** |
| ---------- | --------------- | ------------ |
| Disabled | Disables echo cancellation | Your headset has no audio feedback, you manage echo cancellation externally, or echo cancellation causes issues |
| Mixed echo cancellation | Processes all audio channels together | You use loudspeakers near the microphone or have a headset that "bleeds" audio |
| Multichannel echo cancellation | Processes all audio channels separately, but uses more CPU resources | You use multiple loudspeakers farther away from the microphone |

<br> *Noise Suppression* filters background noise from the audio input. The table below describes the four options for 'Noise Suppression': </br>

| **Option** | **Description** |
| ---------- | --------------- |
| Disabled | Disables noise suppression |
| Speex | Uses Speex's noise suppression algorithm |
| RNNoise | Uses machine learning to reduce noise |
| Both | A combination of RNNoise and Speex |

### Misc

Misc controls cue sounds and the idle action.

![audio-input-misc](images/audio-input-misc.png)

Audio cue sets audio tones for the start and stop of audio transmission. Mute cue sets the audio tone on mute. Idle Action determines Mumble's behavior after the user hasn't interacted with the client (interactions include joining a channel; talking; adding or removing a listener; messaging another user) after a set time (in minutes). The three idle actions are:
* Do nothing
* Mute the microphone
* Deafen the speakers

You can configure Mumble to unset the idle action once you interact with the client again.

## Output

Audio Output controls your speakers. There are five settings:

### Interface

Identical to the setting in Audio Input, but for the speakers.

### Audio Output

Audio Output controls how Mumble handles incoming audio data and speaker volume

![audio-output](images/audio-output.png)

*Default Jitter Buffer* compensates for network jitter. Network jitter measures your connection stability by measuring how much the time delay from packet transmission to delivery varies over a network. Jitter buffers temporarily store incoming audio packets and order them by expected timing values to reduce jitter. *Output Delay* sets the time (in milliseconds) Mumble takes to buffer audio. If you frequently experience audio glitches, such as robotic noise and audio cutoffs, increase *Default Jitter Buffer* and *Output Delay*.

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Increasing both these settings increases the delay between when you talk and when others hear you.</a>
> </div>

### Attenuation

Attenuation controls whether Mumble decreases the volume of other audio outputs when someone talks.

![attenuation](images/attenuation.png)

Attenuation can decrease the volume of local applications in two situations:

* While you talk
* While others talk

You can control how much Mumble lowers the volume of other applications. You can also set Mumble to lower other users' audio streams when you talk as the priority speaker. For users with multiple audio outputs, you can limit attenuation to applications that use Mumble's designated audio output. Finally, you can choose to attenuate PulseAudio loopback modules.

### Positional Audio

Positional Audio transmits the in-game location of every player in a game who is also in the same channel as you.

![positional-audio](images/positional-audio.png)

The table below describes each positional audio option:

| **Option** | **Description** |
| ---------- | --------------- |
| Headphones | Tells Mumble your audio output is a pair of headphones |
| Minimum Distance | The distance another player must be for their volume to start decreasing
| Maximum Distance | The distance another player must be for their volume to reach *Minimum Volume* | 
| Minimum Volume   | The lowest volume other players' volumes will decrease to |
| Bloom | The player volume on all speakers when they're close enough in-game, regardless of their directional position |

### Loopback Test

Loopback Test tests your microphone by playing audio through your speakers. The table below describes the two testing options:

| **Option** | **Description** |
| ---------- | --------------- |
| Local | Tests audio input locally. Provides sliders to simulated packet loss and packet latency |
| Server | Tests audio input using the currently connected server |
