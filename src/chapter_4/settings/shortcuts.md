# Shortcuts

Shortcuts settings lets you set keyboard or mouse button triggers for certain actions.

![shortcuts](images/shortcuts.png)

The table below describes all possible keyboard shortcut functions and their options (if they have any) under the "Data" column:

| **Function** | **Description** | **Data** |
| ------------ | --------------- | -------- |
| Cycle Transmit Mode | Moves through the three transmit modes on every shortcut press | N/A |
| Deafen Self | Blocks both the microphone and speakers from receiving sound | Toggle, On, or Off |
| Hide/show main window | Toggles the Mumble application window showing | N/A |
| Join Channel | Placeholder | N/A |
| Link Channel | Placeholder | N/A |
| Mute Self | Blocks the microphone from transmitting | Toggle, On, or Off |
| Push-to-Mute | Mutes you while you hold the keyboard shortcut | N/A |
| Push-to-Talk | Transmits microphone audio while you hold the keyboard shortcut | N/A |
| Reset Audio Processor | Resets Mumble's audio preprocessor. Use this if your audio isn't transmitting after unplugging and plugging in your microphone | N/A |
| Send Clipboard Text Message | Sends clipboard contents to the active chat target | N/A |
| Send Text Message | Placeholder | N/A |
| Set Transmit Mode to Continuous | Self-explanatory | N/A |
| Set Transmit Mode to Push-to-Talk | Self-explanatory | N/A |
| Set Transmit Mode to VAD | Sets transmit mode to voice activity | N/A |
| Toggle Minimal | Toggles the Mumble minimal user interface | N/A |
| Toggle Overlay | Toggles the in-game overlay |
| Toggle TalkingUI | Self-explanatory | N/A |
| Toggle search dialog | Toggles the channel and user search utility | N/A |
| Unassigned | The NULL action | N/A |
| Unlink Plugin | Placeholder | N/A |
| Volume Down (-10%) | Self-explanatory | N/A |
| Volume Up (+10%) | Self-explanatory | N/A |
| Whisper/Shout | Broadcasts your audio input to set targets | Explained below |

## Whisper/Shout

Mumble allows you to send your audio outside of the channel you're in. When it's to a user or group of users, it's called a whisper; when it's to one or more channels, it's a shout. There are three available 
targets:

* Current Selection
* List of users
* Channel

### Current Selection

*Current Selection* shouts to the actively highlighted channel. In addition, you can choose to have the shout go to the target's subchannel and any channels linked to the target.

![current-selection](images/current-selection.png)

### List of Users

*List of Users* is where you select whisper targets from an available list of users.

![user-list](images/user-list.png)

### Channel

*Channel* lets you target a particular channel to shout to. It also allows filtering based on group. Like *Current Selection*, *Channel* allows you to shout to both the target's subchannels and linked channels.

![channel-shortcuts](images/channel-shortcuts.png)

## Wayland Users

By default, Mumble uses XInput2 to detect keyboard events. Because XInput2 is an Xorg server extension, Wayland users can't use keyboard shortcuts when they don't have the application window in focus. There are two mitigations:

* Force Mumble to use Evdev
* Use global keyboard shortcuts through the wayland compositor or desktop environment (1.4+)

### Force Evdev

Follow these steps:

1. Open `~/.config/Mumble/Mumble.conf` with a text editor
2. Under the [shortcut] section, add the following lines:

    ```
    linux\evdev\enable=true
    linux\evdev\framesperpacket=1
    ```

3. Add your user to the `input` group by running:

    `sudo usermod -aG input insertyourusernamehere`

4. Navigate to the Shortcuts menu in Settings and select the 'Enable Global Shortcuts' bubble

### Global Push-to-Talk

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">This method works for 1.4 or higher only.</a>
> </div>

As of Mumble 1.4, the included DBus API has `startTalk` and `stopTalk` calls. These calls allow you to configure push-to-talk shortcuts at the Wayland compositor or desktop environment level; the compositor sends a DBus message to Mumble when the user presses the set key. For example, if you want to configure push-to-talk globally on sway with the F12 key, enter these two lines:

```
bindsym --whole-window --no-repeat F12 exec dbus-send --session --type=method_call --dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.startTalking
bindsym --whole-window --release F12 exec dbus-send --session --type=method_call --dest=net.sourceforge.mumble.mumble / net.sourceforge.mumble.Mumble.stopTalking
```

If you want to bind to a mouse button, but cannot find its name, run `libinput debug-events` and click the button you want. Its name prints in the terminal output in the fourth column as `BTN_SOMENAME`.
