# Network

The Network section controls connection settings and network related services. It contains four settings.

## Connection

![connection](images/connection.png)

The table below describes each option and their default value:

| **Option** | **Description** | **Default Value** |
| ---------- | --------------- | ----------------- |
| Force TCP Mode | Mumble uses TCP and UDP by default. This disables UDP | <input type="checkbox" disabled> |
| Use Quality of Service | Prioritizes packets using Quality of Service | <input type="checkbox" checked disabled> |
| Reconnect Automatically | Attempts to reconnect to a server when disconnected | <input type="checkbox" checked disabled> |
| Reconnect to last server on startup | Connects to your last joined server on startup | <input type="checkbox" disabled> |
| Suppress certificate and password storage | Tells Mumble not to send your certificate to connected servers and not save passwords | <input type="checkbox" disabled> |

## Proxy

*Proxy* configures the proxy used to connect to Mumble, if any.

![proxy](images/proxy.png)

There are three options:

* Direct Connection
* HTTP(S) proxy
* SOCKS5 proxy

### Direct Connection

Tells Mumble to connect to servers using your Operating System's connection settings.

### HTTP(S) Proxy

Connects to Mumble using a user set HTTP or HTTPS proxy. This option requires the following fields:

* Hostname (the proxy address)
* Port
* Username
* Password

### SOCKS5 Proxy

Connects to Mumble using a user set SOCKS5 proxy. It requires the same input fields as the HTTP(S) proxy.

## Privacy

You can prevent Mumble from reporting your operating system name and version from being reported to servers you connect to.

## Mumble Services

This section has five options:

![mumble-services](images/mumble-services.png)
