# Plugins

Mumble plugins facilitate positional audio in games. Plugins controls the positional audio plugin interface.

![plugins](images/plugins.png)

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">You must enable 'Link to Game and Transmit Position' for any plugin to work. Enabling individual plugins doesn't tell Mumble to enable
>    plugins.</a>
> </div>

The table below describes the three plugin options:

| **Option** | **Description** |
| ---------- | --------------- |
| Enable | Whether to enable that particular plugin |
| PA | Enable positional audio for the plugin |
| KeyEvents | Whether the plugin may receive all keyboard inputs that occur while Mumble is in focus |

<br>Chapter [6.3](../chapter_6/making_plugins.html) discusses how to write plugins for Mumble.</br>
