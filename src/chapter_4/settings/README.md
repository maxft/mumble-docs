# User Settings

This section discusses all of the client configuration options in the settings interface. To access the settings interface, click on the blue gear below the menu bar or 'Configure' > 'Settings'.

![settings](images/settings.png)
