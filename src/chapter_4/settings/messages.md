# Messages

Messages controls how Mumble displays different events to you. 

![messages](images/messages.png)

The table below describes the available message options:

| **Options** | **Description** |
| ----------- | --------------- |
| Console | Displays the event in the client log |
| Notification | Displays a desktop notification |
| Highlight | Highlights application window |
| Text-To-Speech | Delivers message through text-to-speech if it's enabled |
| Limit | Makes event messages conform to the `messagelimit` server parameter after a certain number of users occupy a Mumble server |
| Path | Enables sound notifications |
| Soundfile | Sets the path to the sound file you want to use for that event |

Messages has four sub-settings:

## Text To Speech

*Text To Speech* configures Mumble's text-to-speech engine

![tts](images/tts.png)

*Enable Text-to-Speech* toggles text-to-speech. *Length threshold* sets how many characters the text-to-speech engine will read from a message. *Omit Message Scope* hides the level the message came from, 
such as channel or server. *Omit Message Author* omits the sender of a message. For example, if you enable both *Omit Messsage Scope* and *Omit Message Author*, the text-to-speech engine will only read the message
content. 

## Message Volume

*Message Volume* controls the volume level for the three options shown below:

![message-volume](images/message-volume.png)

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Recall that Audio cue sets audio tones for the start and stop of audio transmission.</a>
> </div>

## Chat Log

*Chat Log* sets client log attributes.

![chat-log](images/chat-log.png)

*Maximum chat length* sets the maximum number of messages the client log displays; if an additional message above the set limit appears in the client log, the first message disappears. *Use 24-hour clock* 
timestamps the log messages with 24-hour time. *Message margins* sets the spacing between log messages.

## Misc.

Misc. sets the message limit threshold as well as the option to limit whispers to only people on your friend list

![messages-misc](images/messages-misc.png)
