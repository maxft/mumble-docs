# Overlay

The Mumble overlay displays useful information to Mumble users playing a game.

![overlay](images/overlay.png)

## Presets

You may load or save presets with custom options for the overlay. Mumble stores these in `.mumblelay` files.

## Layout

This tab has a window previewing the overlay. You can move the elements within the overlay by clicking and dragging the red dot. Right clicking the user list element allows you to edit the list view. There are
four settings:

### Filter

*Filter* controls what users display in the overlay. There are four options:

* Only talking
* Talking and recently active
* All in current channel
* All in linked channels

### Columns

*Columns* sets the number of columns users split into. This is useful when you're in a channel with many users. You can choose between 1 and 5 columns.

### Sort

*Sort* sets how the overlay sorts (in a descending order) the user list. You can sort users alphabetically or by time since last audio transmission. 

### Edit...

*Edit...* opens the overlay editor; this allows you to change the appearance of a user in the list by state.

![overlay-editor](images/overlay-editor.png)

Right-clicking on the user box in the center allows you to change the box layout, opacity, width, fill color, and outline color.

## Overlay Exceptions

This tab lets you choose what applications to allow the overlay to launch with. You can make exceptions in three ways:

### Launch Filter

This is the default option. You can choose game launchers, games, and file or directory paths where mumble overlay will always launch. Additionally, you can add programs for the overlay to ignore in the blacklist.

![exceptions](images/exceptions.png)

### Blacklist

When selected, the overlay launches with every program except those listed.

![blacklist](images/blacklist.png)

### Whitelist

When selected, the overlay launches with listed programs only.

## Linux & BSD

Unlike Windows and Mac OS, the overlay doesn't launch with a game by default. If you installed a binary version of Mumble, run:

`mumble-overlay desiredgame`

in a terminal, and the overlay will launch with `desiredgame`.

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">The <tt><font color="red">mumble-overlay</font></tt> script won't work for steam games.</a>
> </div>

<br>If you installed Mumble from source, run</br>

`LD_PRELOAD=/path/to/libmumbleoverlay.x86_64.so desiredgame`

in a terminal. For 32-bit games, set your `LD_PRELOAD` path to `libmumbleoverlay.x86.so`.

### Steam

To use the overlay within a steam game on Linux, right-click on the game you want and click 'Properties...'. Then, add the following line to launch options:

`LD_PRELOAD=/usr/lib/mumble/libmumble.so.1`

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Make sure to change the path to the mumble overlay library if you installed Mumble from source.</a>
> </div>
