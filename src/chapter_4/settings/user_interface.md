# User Interface

User Interface controls the application's appearance and visual behaviors. This section contains eight settings:

## Layout

You can choose how Mumble displays user interface elements. There are four configurations:

![layout](images/layout.png) 

Classic displays the client log and message bar on the left side, with the channel list of the server displayed on the right side. Stacked vertically aligns the user interface elements, with the channel
list of the connected server on top, the client log in the middle, and the message bar on the bottom. Hybrid is like Classic, but the message bar spans the bottom. Custom allows you to modify one of the three
previously mentioned layouts. You can hide three user interface elements:

* Log
* Chatbar
* Icon Toolbar

You can move the icon toolbar to different places in the user interface. 

## Look and Feel

Look and Feel controls the theme and language that Mumble uses.

![look-and-feel](images/look-and-feel.png)

*Theme* has three presets:

* Mumble - Lite
* Mumble - Dark
* None

On Linux and BSD, 'None' makes Mumble use the system-wide QT theme. You may add 3rd party themes by extracting them to the themes folder. Its location depends on your OS:

* **Linux/BSD**: `~/.local/share/Mumble/Mumble/Themes`
* **Windows**: `C:\Users\<YOURUSERNAME>\AppData\Roaming\Mumble\Themes\`

'Language' changes the language used in the user interface. 'Optimize for high contrast' increases contrast between colors in the user interface to help visually impaired users. 'Show transmit mode dropdown in
toolbar' adds a menu for transmit mode in the Icon toolbar:

![transmit-toolbar](images/transmit-toolbar.png)

## Application

Application controls miscellaneous application behaviors. 

![application-settings](images/application-settings.png)

*Always On Top* sets when Mumble will cover all other application windows. There are four options:

* Never
* Always
* In minimal view
* In normal view

*Show context menu in menu bar* adds user and channel context menus to the Mumble menu bar:

![context-menu](images/context-menu.png)

*Quit Behavior* controls how Mumble responds when you click 'X' on the application window. There are five options:

* Always Ask
* Ask when connected
* Always minimize
* Minimize when connected
* Always Quit

The 'Ask' options make this dialog box appear:

![close-dialog](images/close-dialog.png)

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">This setting doesn't stop you from quitting with Ctrl+Q or by clicking 'Server' > 'Quit Mumble' from the header bar.</a>
> </div>

*Enable Developer menu* adds a context menu to the Mumble menu bar. The table below describes its two items:

| **Item** | **Description** |
| -------- | --------------- |
| Developer Console | Opens a graphical window with time-stamped client logs |
| Positional Audio Viewer | Opens a window with positional audio data |

*Lock layout* prevents you from moving or hiding user interface elements when using the *Custom* layout.

## Tray Icon

Tray Icon controls tray icon behavior. *Hide in tray when minimized* hides the window from the operating system taskbar when you minimize Mumble. *Show talking status in tray icon* makes the tray icon light up when
you're speaking.

![tray-icon](images/tray-icon.png)

## Search

Search controls the action taken when double-clicking on a user or channel in the search dialog.

![search-actions](images/search-actions.png)

The only action is to move into the selected user's channel or the channel you clicked on, respectively.

## Channel Tree

Channel Tree controls how channels in a server display.

![channel-tree](images/channel-tree.png)

*Channel Dragging* sets how Mumble responds when you drag a channel. It has three options:

* Ask
* Move
* Do Nothing

*User Dragging* has the same three options as *Channel Dragging*. *Expand* sets how Mumble unfolds channels. It has three options:

* None
* Only with users
* All

The table below describes the remaining six options:

| **Option** | **Description** |
| ---------- | --------------- |
| Users above Channels | Lists users above subchannels |
| Show channel user count | Shows the number of users in a channel enclosed in parentheses |
| Show volume adjustments | Shows the volume adjustment level (in decibels) enclosed in bars next to the user |
| Show nicknames only | Shows only the nickname of a user if they have one |
| Use selected item as the chat bar target | Automatically switches the chat target to the last clicked channel or user |
| Filter automatically hides empty channels | Self-explanatory |

## Talking UI

Talking UI controls the behavior of the Talking UI.

![talkingui-settings](images/talkingui-settings.png)

The Talking UI is a window with a list of users in the channel you're currently in. It looks like this:

![talking-ui](images/talking-ui.png)

The table below describes all the Talking UI options:

| **Option** | **Description** | **Default Value** |
| ---------- | --------------- | ----------------- |
| Always keep local user visible | Makes the Talking UI show your user even when nobody else is in the channel |  <input type="checkbox" disabled> |
| Abbreviate channel names | Shortens parent channel names exceeding a specified length | <input type="checkbox" checked disabled> |
| Abbreviate current channel name | Shortens current channel names exceeding a specified length | <input type="checkbox" disabled> |
| Show local user's listeners (ears) | Whether to show all your active channel listeners | <input type="checkbox" disabled> |
| Rel. font size (%) | Sets how big the Talking UI font is relative to the rest of the Mumble user interface | `100` |
| Remove silent user after | Sets the number of seconds until Talking UI hides quiet users | `10` |
| Channel hierarchy depth | Number of parent channels Talking UI shows in the current channel name | `1` |
| Max. channel name length | The maximum number of characters that the Talking UI shall display for a channel name | `20` |
| Abbreviated prefix characters | Number of characters to display at the beginning of a shortened channel name | `3` |
| Abbreviated postfix characters | Number of characters to display at the end of a shortened channel name | `2` |
| Abbreviation replacement | The string Talking UI replaces the cut-off portion of a shortened channel with | `...` |

## Channel Hierarchy String

This setting lets you set the string that separates channel names from their parent's channel name.
