# Running Your Server

This chapter explains how to configure server permissions and manage your server day-to-day.
