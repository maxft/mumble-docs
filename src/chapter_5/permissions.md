# Permissions

Permissions grant abilities to users. Mumble handles permissions differently than other programs you may have used before, such as Discord or TeamSpeak. Unlike the permissions TeamSpeak and Discord have, Mumble assigns permissions to groups on the channel level. All sub-channels of a given parent channel inherit the groups of the parent by default. For example, all channels on your server inherit their groups—and hence permissions—from the **Root** channel because they're all children of the **Root** channel.

## SuperUser

When you join a new server, you can't edit anything:

![initial-join](images/initial-join.png)

To make any changes to the server, you need the SuperUser. The SuperUser can do anything on the server except talk. To join your server as SuperUser, do the following:

1. Click on the globe icon in the toolbar
2. Click on *Add New...*
3. Use the SuperUser username and your password, then connect

![superuser](images/superuser.png)

The primary purpose of the SuperUser is editing the Access Control List of the Root channel.

## Access Control Lists

An Access Control Lists (ACL) is a permission list that specifies how users can access objects. In Mumble's case, the objects are channels. To begin, let's right click on the **Root** channel and select *Edit*.

![root-acl-orig](images/root-acl-orig.png)

This is the default Access Control List (ACL) for Mumble. It applies to all channels in the server by default because sub-channels inherit these permissions. The table below describes all ACL permissions:

| **Permission** | **Description** |
| -------------- | --------------- |
| Write ACL | Allows users to edit the ACL |
| Traverse | Allows movement through the channel and its sub-channels. If disabled, users can't access a channel or its sub-channels |
| Enter | Allows entry to a channel |
| Speak | Allows talking in a channel |
| Mute/Deafen | Allows users to mute and deafen one another. It also allows assigning oneself priority speaker |
| Move | Allows a user to move another |
| Make Channel | Allows channel creation |
| Link Channel | Allows the linking of a channel to other channels |
| Whisper | Allows broadcasting audio from other channels |
| Text Message | Allows sending messages in a channel and to other users |
| Make Temporary | Allows creation of channels that delete themselves when all users leave them |
| Listen | Allows channel listeners |
| Kick | Allows a user to forcefully disconnect another |
| Ban | Allows the removal of a user for a given time |
| Register User | Allows a user to register another with the server |
| Register Self | Allows a user to register themselves with the server |
| Reset User Content | Allows a user to reset another's avatar and comment |

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Mumble applies permissions from top to bottom. This means entries further down the list overwrite permissions set in entries above 
>    them if there are contradictions.</a>
> </div>

## Groups

ACLs bind permissions to groups. The table below describes the three default groups:

| **Group** | **Description** |
| --------- | --------------- |
| `all` | All users on a server |
| `auth` | All registered users |
| `admin` | Has power to modify the server-wide ACL by default. You'll need to assign this to a normal user if you want them to make any changes to the server |

To add groups, right click on the root channel. Then, select the *Groups* tab:

![groups-tab](images/groups-tab.png)

To create a group, type the desired group name in the drop-down box below the **Group** section header. Groups have six data:

* **Members**: List of users in the group
* **Inherited Members**: List of members inherited from the group in the parent channel when dealing with sub-channels
* **Excluded Members**: List of inherited members to drop from the group
* **Inherit**: Whether the group inherits members from the parent channel
* **Inheritable**: Whether to make the group available in sub-channels
* **Inherited**: Whether the channel inherited the group from a parent channel

To add yourself to a group, click on the drop-down box and select the group you want, such as `admin`. Then, enter the name of your user into the text box under Members and click *Add*.

![admin-add](images/admin-add.png)

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">A user must register with the server to be added to any group.</a>
> </div>

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Assigning permission to a group lets all group members use that permission on anyone, including other group members.</a>
> </div>

## Channels

To add channels, right-click the root channel and select *Add...*. Then, configure the options you want in the dialog and click *OK*:

![add-channel-dialog](images/add-channel-dialog.png)

You may change the ACL for your newly created channel. Right click it and select *Edit*. When you click on the ACL tab, you will notice that your new channel inherits the ACL of the root channel by default:

![channel-acl](images/channel-acl.png)

Remember that Mumble interprets ACLs from top to bottom. Therefore, any new entries you make below the inherited entries change the permissions of that channel from the server-wide defaults.

### Restricted Access Channels

You may want to create channels that only some people can access. Mumble provides two methods for this:

* Passwords
* ACL restrictions

<h4>Passwords</h4>

After creating a channel, right-click it and select *Edit...*. You can set a password under the properties tab:

![set-password](images/set-password.png)

To allow users to access, tell them the password. Then, they can click on *Server* in the menu bar and select *Access Tokens*. From there, click on *Add* and enter the password for that channel:

![access-tokens](images/access-tokens.png)

On passworded channels, you shall see a red or green lock icon depending on whether you have the password stored in *Access Tokens*:

![lock-icons](images/lock-icons.png)

<h4>ACL restrictions</h4>

This may be a better option if you want to restrict access by existing groups. Create the channel you want and edit it. Then, create an entry for `all` and set *Deny* privileges on the following:

![restricted](images/restricted.png)

Then, create a group that may access the channel, such as `allowed`. Create an entry for `allowed` and set *Allow* privileges on the following:

![allowed](images/allowed.png)

Now, only users in `allowed` and `admin` can join this channel.

### Channel Groups

Mumble designates channel groups with the tilde (~) prefix. The table below describes the three channel-specific groups Mumble provides:

| **Channel Group** | **Description** |
| ----------------- | --------------- |
| `~in` | All users inside a given channel |
| `~out` | All users outside a given channel |
| `~sub` | Sets permissions for sub-channels sharing a common parent |

You can pass positional arguments to `~sub` that limit the applied scope relative to the channel where the `~sub` entry is; the format is `~sub,a,b,c`. The table below explains these positional arguments:

| **Positional Argument** | **Description** |
| ----------------------- | --------------- |
| *a* | Minimum Number of common parents |
| *b* | The minimum channel depth the sub-channel must be from its parent |
| *c* | The maximum channel depth the sub-channel must be from its parent |

The values of the arguments are always positive integers, except for *a*: setting -1 for *a* makes the `~sub` permissions apply to the channel the administrator set it in.
