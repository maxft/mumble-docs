# Moderation

This section discusses the tools Mumble provides for moderation outside of permissions.

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">This section assumes you have the required permissions to carry out these actions.</a>
> </div>

## Kick

To kick a user, right-click their name and select *Kick…*. This will forcefully disconnect them from the server and prevent them from automatically reconnecting.

## Ban

To ban a user, right-click their name and select Ban. Banning a user disconnects them from the server and prevents them from reconnecting for a period that you set.

### Ban List

To access the ban list, click *Server* in the menu bar and select *Ban List…*:

![access-ban-list](images/access-ban-list.png)

The ban list shows all banned users and allows those with ban power to modify the parameters around a particular ban:

![ban-list](images/ban-list.png)

When you ban someone, Mumble automatically fills these fields for you. You can manually add an entry to the ban list if you know the user's parameters.

## Registered Users

To see a list of the registered users on your server, click *Server* in the menu bar and select *Registered Users…*:

![access-registered-users](images/access-registered-users.png)

From the registered users interface, you can change a user's name and remove them from the registered list:

![registered-users](images/registered-users.png)
