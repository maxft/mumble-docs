# Pre-Installation Considerations

Consider these questions before proceeding with installing the Mumble server (Murmur):

1. Where am I going to host the server?
    * A managed Mumble provider 
    * Self-hosted on a VPS or dedicated server
2. What operating system do I want to use?

A VPS or dedicated server is the best option for people who value privacy, security, and control. Managed Mumble providers may be better for people with small communities or who don't want to be system administrators. It's best practice to run Murmur on a Linux distribution or a BSD because they're stable and secure; they make great server operating systems. Furthermore, Murmur runs as a system daemon on Linux and BSD: perfect for people running their servers 24/7.

# Installation

This section contains operating system specific instructions for installation

## Windows

The version 1.3 wizard allows users to select Murmur as an installation component. As of 1.4, users download Murmur from [here](https://www.mumble.info/downloads/).

![Murmur in the 1.3 installation wizard](images/windows-murmur.png)

## Mac OS

Users can download Murmur [here](https://www.mumble.info/downloads/). 

## Linux

Install `murmur` from your package manager. On Debian and Ubuntu, the package is called `mumble-server`.

### Firewall Configuration

Install a firewall if you don't already have one. On Linux, nftables is the default firewall for the Linux kernel. If you aren't comfortable with nftables syntax, you may prefer ufw. On Debian and Ubuntu, for example, we'd run the following commands to install ufw and allow for traffic through Murmur's default port:

```bash
sudo apt install ufw
sudo systemctl enable ufw.service && sudo systemctl start ufw.service
sudo ufw allow 64738
sudo systemctl restart ufw.service
```

For nftables:

```bash
sudo apt install nftables
sudo systemctl enable nftables.service && sudo systemctl start nftables.service
```

Then, run `sudo nano /etc/nftables.conf` and paste this inside:

```bash
#!/usr/sbin/nft -f

flush ruleset

# -- IPv4 --
table ip filter {
        chain INPUT {
                type filter hook input priority 0; policy drop; # by default, we drop traffic

                iif lo accept comment "Accept any localhost traffic"
                ct state invalid counter drop comment "Drop invalid connections"
                ct state { established, related } counter accept comment "Accept traffic originated from us"
                iif != lo ip daddr 127.0.0.1/8 counter drop comment "drop connections to loopback not coming from loopback"

                ip protocol icmp icmp type { destination-unreachable, router-solicitation, router-advertisement, time-exceeded, parameter-problem } accept comment "Accept ICMP"

                tcp dport { ssh, 64738 } counter accept comment "Accept ssh and mumble"
                udp dport { 64738 } counter accept comment "Accept mumble"
                counter drop
        }
        chain FORWARD {
                type filter hook forward priority 0; policy drop;
                counter comment "count dropped packets"
        }
        chain OUTPUT {
                type filter hook output priority 0; policy accept;
        }
}

# -- IPv6 --
table ip6 filter {
        chain INPUT {
                type filter hook input priority 0; policy drop; #by default, we drop traffic

                iif lo accept comment "Accept any localhost traffic"
                iif != lo ip6 daddr ::1/128 counter drop comment "drop connections to loopback not coming from loopback"
                ct state invalid drop comment "Drop invalid connections"
                ct state established,related accept comment "Accept traffic originated from us"

                ip6 nexthdr icmpv6 icmpv6 type { destination-unreachable, packet-too-big, time-exceeded, parameter-problem, mld-listener-query, mld-listener-report, mld-listener-reduction, nd-router-solicit, nd-router-advert, nd-neighbor-solicit, nd-neighbor-advert, ind-neighbor-solicit, ind-neighbor-advert, mld2-listener-report } accept comment "Accept ICMPv6"

                tcp dport { ssh, 64738 } counter accept comment "Accept ssh, mumble"
                counter drop
        }
        chain FORWARD {
                type filter hook forward priority 0; policy drop;
                counter comment "count dropped packets"
        }
        chain OUTPUT {
                type filter hook output priority 0;
        }
}
```

Then, restart the nftables daemon. On systemd based distributions, run:

`sudo systemctl restart nftables.service`

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Increasing both these settings increases the delay between when you speak and when others hear you.</a>
> </div>

## FreeBSD

Run `sudo pkg install murmur`. Alternatively, build the port by running `cd /usr/ports/audio/murmur && sudo make install clean`

### Firewall Configuration

FreeBSD uses [ipfw](https://docs.freebsd.org/en/books/handbook/firewalls/#firewalls-ipfw) by default. You can also use [pf](https://docs.freebsd.org/en/books/handbook/firewalls/#firewalls-pf), but it 
differs significantly from the OpenBSD version. To configure ipfw, run:

```bash
sudo sysrc firewall_enable="YES"
sudo sysrc firewall_type="workstation"
sudo sysrc firewall_quiet="YES"
sudo sysrc firewall_allowservices="any"
sudo sysrc firewall_logdeny="YES"
sudo sysrc firewall_myservices="64738"
```

To open the UDP port, run `sudo vi /etc/rc.firewall`. Go past the following section:

```bash
for i in ${firewall_allowservices} ; do
  for j in ${firewall_myservices} ; do
    ${fwcmd} add pass tcp from $i to me $j
  done
done
```

Then, insert the following line:

`${fwcmd} add pass udp from any to me 64738`

Now, run `sudo service ipfw start`.

# Starting Murmur

On Linux and FreeBSD, start the daemon after you've configured the firewall. On systemd based Linux distributions, run:

`sudo systemctl enable murmur.service && sudo systemctl start murmur.service`

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Make sure to replace <tt><font color="red">murmur</font></tt> with <tt><font color="red">mumble-server</font></tt> on Debian and 
>    Ubuntu.</a>
> </div>

On FreeBSD and NetBSD, run:
`sudo service murmur start`

## Launch Options

It's best to run Murmur as a system daemon rather than directly, but Murmur has a few options that must be passed to it through command flags. The binary is called `murmurd` and its command line flags are:

* **-h, -help, --help** - Retrieves a list of commandline flags
* **-ini** ***inifile*** - Specifies an ini file path if you want to change the default location
* **-fg** - Run in the foreground
* **-v** - Logs more verbosely
* **-supw** ***password*** ***serverid*** - Sets the SuperUser password for a server
* **-readsupw** ***serverid*** - Sets the SuperUser password via a stdin prompt 
* **-disablesu** - Disables the SuperUser Account
* **-limits** - Makes Murmur show how many clients it can handle and exits
* **-wipessl** - Wipes SSL certificates from the database
* **-wipelogs** - Wipes all logs from the database
* **loggroups** - Turns on logging for group changes for all servers (version 1.4+)
* **logacls** - Turns on logging for ACL changes for all servers (version 1.4+)
* **-version, --version** - Shows version information
* **-license** - Shows license information (version 1.4+)
* **-authors** - Lists Murmur's authors (version 1.4+)

> <div class="warning box">
>     <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">The serverid field is necessary only if you're running multiple virtual servers.</a>
> </div>
