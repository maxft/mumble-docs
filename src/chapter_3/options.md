# The Configuration File

The murmur configuration file, `murmur.ini`, has the following default locations:

* **Windows**: `C:\Program Files\Mumble\murmur.ini`
* **Linux**: `/etc/murmur.ini`
    * **Debian/Ubuntu**: `/etc/mumble-server.ini`
* **FreeBSD**: `/usr/local/etc/murmur.ini`

## Options

This table contains all configuration options along with an explanation of each:

| **Option** | **Description** | **Data Type** | **Example** |
| ---------- | --------------- | ------------- | ----------- |
| `welcometext` | Welcome message sent to clients when they connect. You may use the QT HTML and CSS [subset](https://doc.qt.io/qt-5/richtext-html-subset.html) | `string` | `welcometext="\<b>Hello\</b> \<i>User!\</i>"` |
| `welcometextfile` | Murmur reads the text file at the set path. Introduced in 1.4 | `string` | `welcometextfile=welcome.txt` |
| `port` | The network port mumble broadcasts on | `integer` | `port=64738` |
| `host` | The IP address or hostname murmur binds to | `string` | `host=127.0.0.1` |
| `serverpassword` | Murmur asks for this password if you fill this option. Leave it blank if you don't want a server password | `string` | `serverpassword=thosearemycookies` |
| `bandwidth` | The maximum bandwidth clients can use (measured in bits per second). Values above 230400 are irrelevant | `integer` | `bandwidth=128000` |
| `users` | Maximum number of users allowed on a server simultaneously | `integer` | `users=1000` |
| `usersperchannel` | Maximum number of users allowed in one channel simultaneously | `integer` | `users=8` |
| `registerName` | Names the root channel | `string` | `registerName=Digital Cafe` |
| `registerPassword` | The password used to create a registry for the server name | `string` | `registerPassword=bishbashbosh` |
| `registerUrl` | Your website | `string` | `https://example.com` |
| `registerHostname` | Your domain name for the server. Only set this for a server with a static IP | `string` | `mumble.example.com` |
| `registerLocation` | Sets the country that the server appears under in the public server list. Uses [Country Codes](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) | `string` | `registerLocation=UK` |
| `uname` | The user murmur switches to if you start as the root user | `string` | `uname=murmur_user` |
| `certrequired` | Whether murmur allows clients with certificates only | `boolean` | `certrequired=true` |
| `sendversion`  | Whether murmur sends clients information about its version and operating system | `boolean` | `sendversion=true` |
| `logfile` | Path to the log file | `string` | `logfile=/var/log/murmur/murmur.log` |
| `logdays` | Number of days murmur retains its log entries within its internal database. 0 keeps logs forever and -1 disables logging | `integer` | `logdays=21` |
| `loggroupchanges` | Whether to log all groups and their members from before and after a group in a channel changes. Introduced in 1.4 | `boolean` | `loggroupchanges=true` |
| `logaclchanges` | Whether to log all ACLs from before and after an ACL change in a channel. Introduced in 1.4 | `boolean` | `logaclchanges=true` |
| `pidfile` | Path where mumble writes its process ID to when running as a daemon | `string` | `pidfile=/var/run/murmur/murmur.pid` |
| `autobanAttempts` | Number of login attempts allowed from one IP | `integer` | `autobanAttemps=7` |
| `autobanTimeframe` | Duration in seconds for autobanAttempts | `integer` | `autobanTimeframe=60` |
| `autobanTime` | Duration in seconds of the automatic ban | `integer` | `autobanTime=600` |
| `autobanSuccessfulConnections` | Whether to avoid banning successful connections from the same IP address. Introduced in 1.4 | `boolean` | `autobanSuccessfulConnections=false` |
| `opusthreshold` | Number of users required for Opus to activate. 0 always enables it | `integer` | `opusthreshold=0` |
| `channelnestinglimit` | Maximum level of channel nesting | `integer` | `channnelnestinglimit=6` |
| `channelname` | Uses [regular expressions](https://doc.qt.io/qt-5/qregexp.html#characters-and-abbreviations-for-sets-of-characters) to restrict channel names | `string` | `channelname=[ \\\\-=\\\w\\\\#\\\\[\\\\]\\\\{\\\\}\\\\(\\\\)\\\\@\\\\\|]+` |
| `username` | Uses [regular expression](https://doc.qt.io/qt-5/qregexp.html#characters-and-abbreviations-for-sets-of-characters) to restrict usernames | `string` | `username=[-=\\\w\\\\[\\\\]\\\\{\\\\}\\\\(\\\\)\\\\@\\\\\|\\\\\.]+` |
| `textmessagelength` | Maximum character length for text messages. 0 sets no limit | `integer` | `textmessagelength=2500` |
| `imagemessagelength` | Maximum character length for images. 0 sets no limit | `integer` | `imagemessagelength=0` |
| `allowhtml` | Whether clients may use HTML in messages, user comments, and channel descriptions | `boolean` | `allowhtml=yes` |
| `allowping` | Exposes the current user count, maximum user count, and maximum bandwidth per client to unauthenticated users | `boolean` | `allowping=true` |
| `timeout` | Murmur will disconnect all clients that haven't communicated with the server for this many seconds | `integer` | `timeout=45` |
| `rememberchannel` | Whether Murmur automatically connects clients to the channel they were last in before they disconnected. Introduced in 1.4 | `boolean` | `rememberchannel=true` |
| `rememberchannelduration` | Number of seconds Murmur remembers the last channel of a user. 0 makes Murmur remember forever | `integer` | `rememberchannelduration=86400` |
| `defaultchannel` | The ID of the channel that users shall connect to if they haven't connected to the server before or `rememberchannel=false` | `integer` | `defaultchannel=1` |
| `sslCert` | The path to the key for a SSL certificate | `string` | `sslCert=/path/to/cert.pem` |
| `sslKey` | The path to the key for a SSL certificate | `string` | `sslKey=/path/to/key.pem` |
| `sslpassphrase` | Password for `sslKey` if it's encrypted with a password | `string` | `sslpassphrase=supersecure` |
| `sslCiphers` | Sets the cipher suites that Murmur uses for SSL/TLS in [OpenSSL cipher notation](https://www.openssl.org/docs/apps/ciphers.html#CIPHER-LIST-FORMAT) | `string` | `sslCiphers=TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256` |
| `sslDHParams` | The path to a PEM-encoded file with Diffie-Hellman parameters or named parameters | `string` | `sslDHParams=dh.pem` </br> `sslDHParams=@ffdhe4096` |
| `sslCA` | Path to an [intermediate certificate](https://www.godaddy.com/help/what-is-an-intermediate-certificate-868) if your certificate authority uses one | `string` | `sslCA=/path/to/class1.intermediate.pem` |
| `legacyPasswordHash` | Sets password hash storage to legacy mode. Do not enable! | `boolean` | `legacyPasswordHash=false` |
| `kdfIterations` | Manually sets the number of PBKDF2 iterations. Values less than 1 lets Murmur automatically choose | `integer` | `kdfIterations=65536` |
| `bonjour` | Enables the bonjour auto-discovery service | `boolean` | `bonjour=false` |
| `suggestVersion` | Murmur notifies users in their client log if their client version is below this version. Works on client versions >=1.2.4 | `string` | `suggestVersion=1.3.0` |
| `suggestPositional` | Whether Murmur notifies users in their client log if they don't have positional audio enabled. Works on client versions >=1.2.4 | `boolean` | `suggestPositional=true` |
| `suggestPushToTalk` | Whether Murmur notifies users in their client log if they don't have push-to-talk enabled. Works on client versions >=1.2.4 | `boolean` | `suggestPushToTalk=true` |
| `obfuscate` | Whether Murmur randomizes client IPv4 connections in the log file | `boolean` | `obfuscate=true` |
| `listenersperchannel` | Number of listener proxies in one channel. -1 allows infinite listener proxies and 0 disables them. Introduced in 1.4 | `integer` | `listenersperchannel=3` |
| `listenersperuser` | Number of listener proxies one user can have. -1 allows infinite proxies and 0 disables them. Introduced in 1.4 | `integer` | `listenersperuser=2` |
| `messagelimit` | Number of allowed messages per second. Value must be greater than 0 | `integer` | `messagelimit=1` |
| `messageburst` | Number of allowed messages in a short burst. Value must be greater than 0 | `integer` | `messageburst=5` |
| `database` | Path to the database | `string` | `database=/var/db/murmur/murmur.sqlite` |
| `sqlite_wal` | 0 uses SQLite's default rollback journal, 1 uses write-ahead log with synchronous=NORMAL, and 2 uses write-ahead log with synchronous=FULL | `integer` | `sqlite_wal=1` |
| `dbDriver` | Sets the database backend Murmur uses. Here are the available [options](https://doc.qt.io/qt-5/sql-driver.html) | `string` | `dbDriver=QSQLITE` |
| `dbUsername` | The username for the database | `string` | `dbUsername=john_doe` |
| `dbPassword` | The password for the database | `string` | `dbPassword=topsecret` |
| `dbHost` | The IP address or host name the database binds to | `string` | `dbHost=127.0.0.1` |
| `dbPort` | The network port the database binds to | `integer` | `dbPort=3306` |
| `dbPrefix` | The prefix applied to automatically created tables and sequences in the database | `string` | `dbPrefix=murmur_` |
| `dbOpts` | Passes [supported options](https://doc.qt.io/qt-5/qsqldatabase.html#setConnectOptions) to the database | `string` | `dbOpts="UNIX_SOCKET=/var/lib/mysql/mysql.sock"` |
| `dbus=session` | Insert this line to use the deprecated D-Bus Remote Procedure Call (RPC) interface | `N/A` | `N/A` |
| `dbusservice` | Sets a distinct service name for this Murmur process on the D-Bus. Don't use this unless you have multiple Murmur instances on the same D-Bus | `string` | `dbusservice=net.sourceforge.mumble.murmur` |
| `ice` | Enables [Ice](https://zeroc.com/products/ice) and sets its endpoint | `string` | `ice="tcp -h 127.0.0.1 -p 6502"` | 
| `icesecretread` | Sets a passphrase that scripts must possess when read accessing Ice | `string` | `icesecretread=canisee` |
| `icesecretwrite` | Sets a passphrase that scripts must possess when write accessing Ice | `string` | `icesecretwrite=caniwrite` |
| `Murmur.PublishedEndpoints` | Sets a published endpoint when using Ice behind a NAT. Must be at the bottom of murmur.ini under [Ice] | `string` | `Murmur.PublishedEndpoints=tcp -h 123.4.1.1 -p 7000` |
| `grpc` | Sets the IP address for Murmur's experimental gRPC API to bind on | `string` | `grpc="127.0.0.1:50051"` |
| `grpccert` | Sets the certificate file needed for SSL/TLS support | `string` | `grpccert=/path/to/cert.pem` |
| `grpckey` | Sets the key file needed for SSL/TLS support | `string` | `grpckey=/path/to/key.pem` |
| `grpcauthorized` | Space delimitted list of the SHA256 hashes for authorized client certificates | `string` | `grpcauthorized=sha256hash1 sha256hash2` |
