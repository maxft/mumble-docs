# Server Scripting

Murmur can receive commands from third-party programs called Remote Procedure Call (RPC) frameworks. It's like a television clicker for Murmur: you send commands using an RPC framework, and Murmur will execute them. Server scripting allows for both direct commands and event-based commands.

## Capabilities

Some functions that server scripting makes possible are:

* Authenticators that allow users to log in with credentials from an existing database
* Additional context (right-click) menu actions
* Adjustments to users based on specified events
* Online channel viewers
* Web Interfaces for server management

## Remote Procedure Call (RPC) Frameworks

The table below lists the four available RPC frameworks Murmur supports and where their documentation is:

| **RPC Framework** | **Status** | **Documentation** |
| ----------------- | ---------- | ----------------- |
| Ice | Stable | [Ice documentation](https://doc.zeroc.com/), [Murmur SLICE types](https://www.mumble.info/documentation/slice/1.3.0/html/Murmur.html)
| DBus | Deprecated & Incomplete | See Below |
| gRPC | In development & Incomplete | [gRPC guides](https://grpc.io/docs/guides/), [Go implementation of gRPC](https://pkg.go.dev/google.golang.org/grpc) |

### DBus

After you enable DBus by setting the `dbus=session` option in murmur.ini, run `murmurd -fg -v` and look for the line: "DBus registration succeeded." To list all available DBus methods, run:

`dbus-send --system --dest=net.sourceforge.mumble.murmur --type=method_call --print-reply / org.freedesktop.DBus.Introspectable.Introspect`

The format for sending commands with DBus is:

`dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.<interface>.<method> <type>:<value>`

The table below describes each DBus method with examples:

| **Method** | **Description** | **Type** | **Example**| 
| ---------- | --------------- | -------- | ---------- |
| `getAllServers` | Lists all virtual servers | `N/A` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.getAllServers` |
| `getBootedServers` | Lists all active virtual servers | `N/A` |`dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.getBootedServers` |
| `isBooted` | Checks whether a virtual server is active | `int` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.isBooted int32:<serverid>` |
| `start` | Starts a specified virtual server | `int` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.start int32:<serverid>` |
| `stop` | Stops a specified virtual server | `int` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.stop int32:<serverid>` |
| `newServer` | Creates a virtual server | `N/A` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.newServer` |
| `deleteServer` | Deletes a specified virtual server | `int` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.deleteServer int32:<serverid>` |
| `getConf` | Gets the value of a given configuration option from a specified virtual server | `int,string` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.deleteServer int32:<serverid> string:"<option>"` |
| `getAllConf` | Gets all configuration options for a specified virtual server | `int` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.getAllConf int32:<serverid>` |
| `getDefaultConf` | Gets the default configuration options | `N/A` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.getDefaultConf` |
| `setConf` | Sets a specified configuration option | `int,string` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.setConf int32:<serverid> string:"<key>" string:"<value>"` |
| `setSuperUserPassword` | Sets the SuperUser password on a specified virtual server | `int,string` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.setSuperUserPassword int32:<serverid> string:"<password>"` |
| `rotateLogs` | Makes Murmur rotate the server logs | `N/A` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.rotateLogs` |
| `getLog` | Gets the log of a given virtual server within a specified timeframe | `int` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.getLog int32:<serverid> int32:<min_seconds> int32:<max_seconds>` |
| `quit` | Quits Murmur | `N/A` | `dbus-send --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call / net.sourceforge.mumble.Meta.quit` |
| `getPlayers` | Gets a list of users' information | `N/A` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.getPlayers` |
| `getChannels` | Gets a list of information about all channels | `N/A` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.getChannels` |
| `getACL` | Gets the Access Control List information of a specified channel | `int` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.getACL int32:<channelid>` |
| `setACL` | Sets an ACL entry for a specified channel | `int` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.setACL int32:<channelid>` |
| `getBans` | Gets all active bans on a server | `N/A` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.getBans` |
| `setBans` | Bans a user | `int` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.setBans long:<ipaddress> int32:<bits>` |
| `getPlayerState` | Prints a shortened list of a specified user's information | `int` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.getPlayerState uint32:<sessionid>` |
| `setPlayerState` | Sets a specified player's attributes | `long,bool,int` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.setPlayerState long:<sessionid> boolean:<mute> boolean:<deaf> boolean:<suppressed> boolean:<selfMute> boolean:<selfDeaf> int32:<channelid>` |
| `getPlayerNames` | Gets a list of users' names associated with their session IDs | `int` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.getPlayerNames uint32:<sessionid>` |
| `getPlayerIds` | Gets a list of users' session IDs associated with their names | `string` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.getPlayerIds string:"<playername>"` |
| `addChannel` | Creates a new channel | `string,int` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.addChannel string:"<channelname>" int32:<parentchannelid>` |
| `removeChannel` | Deletes a specified channel | `int` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.removeChannel int32:<channelid>` |
| `setChannelState` | Sets a channel's ID, name, parent, and linked channels | `string,int,array` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.setChannelState int32:<channelid> string:<channelname> int32:<parentid> array:int32:<linkedchannel1id>,<linkedchannel2id>` |
| `registerPlayer` | Registers a username to a server | `string` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.registerPlayer string:"<username>"` |
| `getRegistration` | Gets registration information from a user ID | `int` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.getRegistration int32:<userid>` |
| `getRegisteredPlayers` | Gets registration information from a username | `string` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.getRegisteredPlayers string:"<username>"` |
| `setRegistration` | Updates a user's registration information | `int,string` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.setRegistration int32:<userid> string:"<username>" string:"<email>" string:"<password>"` |
| `kickPlayer` | Kicks a user | `int,string` | `dbus-send --system --print-reply --dest=net.sourceforge.mumble.murmur --type=method_call /<serverid> net.sourceforge.mumble.Murmur.kickPlayer uint32:<sessionid> string:"<reason>"` |

### Ice

Ice is the preferred method for communication with Murmur. The configuration file enables Ice by default with the line: `ice="tcp -h 127.0.0.1 -p 6502"`. To verify that Murmur is listening on the network port
you specified, run `ss -tln | grep 6502` for Linux or `sockstat -l -P tcp | grep 6502` for FreeBSD and NetBSD. The Murmur log also shows this line if Ice is functioning correctly:

`MurmurIce: Endpoint "tcp -h 127.0.0.1 -p 6502" running`

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">If Ice isn't working, comment out <tt><font color="red">icesecretwrite</font></tt> in <tt><font color="red">murmur.ini</font></tt>.</a>
> </div>

Ice uses an Interface Description Language [(IDL)](https://en.wikipedia.org/wiki/Interface_description_language) called the Specification Language for Ice (SLICE). The Mumble GitHub repository contains a SLICE [file](https://github.com/mumble-voip/mumble/blob/master/src/murmur/Murmur.ice)
with a list of all Ice interfaces for Murmur. Ice has compiler tools to convert a SLICE file to a file of a specified language. The Ice documentation discusses SLICE compilers [here](https://doc.zeroc.com/ice/3.7/the-slice-language/using-the-slice-compilers).

For example, if you want to convert the Murmur SLICE definitions to a python module, run:

`slice2py --checksum -I/usr/local/share/Ice -I/usr/share/Ice/slice -I/usr/share/ice/slice -I/usr/share/slice Murmur.ice`

The `-I` flag includes the necessary directories for the conversion. Now you can write a script that interacts with Ice. For example, this script lists all virtual servers for your Murmur instance:

```python
#!/usr/bin/env python2

import sys, Ice
import Murmur

with Ice.initialize(sys.argv) as communicator:
    base = communicator.stringToProxy("Meta:tcp -h 127.0.0.1 -p 6502")

    meta = Murmur.MetaPrx.checkedCast(base)
    if not meta:
        raise RuntimeError("Invalid proxy")

    servers = meta.getAllServers()

    if len(servers) == 0:
        print("No servers found")

    for currentServer in servers:
        if currentServer.isRunning():
            print("Found server (id=%d):\tOnline since %d seconds" % (currentServer.id(), currentServer.getUptime()))
        else:
            print("Found server (id=%d):\tOffline" % currentServer.id())
```

<h4>Mumo</h4>

The Mumble development team provides a tool called mumo (Mumble Moderator). It's a frontend for mumo that uses Python plugins, called modules, that interact with Ice. To obtain mumo, run the following:

```bash
sudo pip install zeroc-ice # Use the python 2 pip
git clone https://github.com/mumble-voip/mumo.git && cd mumo
mkdir modules-enabled
```

To enable a module, make a symbolic link from its ini file in the `modules-available` directory to the `modules-enabled` directory:

`ln -s /path/to/mumo/modules-available/module_name.ini /path/to/mumo/modules-enabled/module_name.ini`

Although you can start mumo by running `./mumo.py`, it's best to run it as a system daemon. Here's a systemd unit for it:

```ini
[Unit]
Description=Mumble Moderator
After=network.target

[Service]
User=yourusername
WorkingDirectory=/path/to/mumo/
# Change /usr/bin/python2.7 to the location of the python 2 binary
ExecStart=/usr/bin/python2.7 mumo.py -i mumo.ini --app 
Restart=always

[Install]
WantedBy=multi-user.target
```

Save it in the /etc/systemd/system directory as `mumo.service` and run `sudo systemctl daemon-reload`. Now, run `sudo systemctl enable mumo.service && sudo systemctl start mumo.service`. Here's a list of third-party mumo modules:

* [chatimg](https://github.com/aselus-hub/chatimg-mumo)
* [messageforwarder](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/messageforwarder)
* [wrongversion](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/wrongversion)
* [welcomemessage](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/welcomemessage)
* [userinfo](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/userinfo)
* [sticky](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/sticky-contextmenu)
* [setstatus](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/setstatus)
* [registerusers](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/registerusers-contextmenu)
* [ossuffixes](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/os-suffixes)
* [lowbw](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/lowbandwidth-channel)
* [getsupport](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/getsupport)
* [deaftoafk](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/deaftoafk)
* [antirec](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/antirec-contextmenu)
* [antiflood](https://github.com/Natenom/mumblemoderator-module-collection/tree/master/antiflood)
* [maxusers](https://github.com/ExplodingFist/mumo-maxusers/)
* [opcommand](https://github.com/ExplodingFist/mumo-opcommand)
* [chatlogger](https://github.com/braandl/chatlogger-for-mumo)

### gRPC

gRPC is a Remote Procedure Call (RPC) framework developed by Google. gRPC uses [protobuf](https://developers.google.com/protocol-buffers/) as its interface description language. 
