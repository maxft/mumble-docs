## Root Channel Setup

The root channel ACL sets the server-wide permissions.

It's best to alter the default. Let's start with removing the `@auth` entry. Now, set *Deny* on the bottom `@all` entry except for *Traverse* and *Register Self*. Finally, set the context to apply to sub-channels. 
Your Root ACL will look like this:

![root-acl](./images/root-acl.png)

Mumble applies permissions from top to bottom. Therefore, entries further down the list overwrite permissions set in entries above them if there are contradictions. Here, the bottom `@all` entry overwrites the top
`@all` entry and denies most permissions by default in all created channels. By doing this, the server administrator can enable permissions channel-by-channel. 

Reserve the built-in admin group for the server owner. Add a new entry and set the group to `admin`. Make it look like this:

![root-admin-privs](./images/root-admin-privs.png)

To add yourself to admin, click the 'Groups' tab in the editor. Then, click on the group list drop-down box and select `admin`. Finally, enter the name of your user into the text box under Members and click 
*Add*.

![admin-add](./images/admin-add.png)

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">A user must register with the server to have groups assigned to them.</a>
> </div>

Many server administrators overseeing larger servers want designated users to help with moderation. In the edit dialog box, click the *Groups* tab. You may choose any group name, but we'll pick 
`moderator`. Type this name into the group list box and click *Add*:

![moderator-add](./images/moderator-add.png)

You can add users to this group later, but let's return to the *ACL* tab for now. Click *Add*. Under *User/Group*, click the group drop-down and select `moderator` or the group name you created. You may alter the permissions suggested below, but this is a good example:

![root-mod-privs](./images/root-mod-privs.png)

It's best to disallow all users from accessing the root channel. Users in `moderator` and `admin` can join the root channel right now. To prevent this, make two new entries for moderator and admin. Make them look like this:

![admin-root-deny](./images/admin-root-deny.png)

![mod-root-deny](./images/mod-root-deny.png)

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Make sure to create a new channel as <tt><font color="red">admin</font></tt> before blocking access completely; right-click the root 
>    channel and click on <i>Add...</i>.</a>
> </div>

## Channels

When you create a new channel, remember to add a channel-specific entry for the `all` group so people can access the channel.

![all-channel-entry](./images/all-channel-entry.png)

## Example Configurations

Let's explore some different ways we can leverage ACLs.

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">These examples assume you have an entry for the <tt><font color="red">all</font></tt> group like the one done in the
>    Root Channel Setup section.</a>
> </div>

### AFK Channel

A good AFK channel automatically mutes players when they enter and un-mutes them when they exit. Add a new entry for the channel ACL with the `all` group with the Enter permission set to *Allow*

### Prevent Audio Transmission between linked Sub-channels

Let's say you are playing Counter-Strike: Global Offensive, a competitive multiplayer First Person Shooter (FPS). We don't want players to hear the audio from the other team, but we want the parent channel to 
hear all audio from both team channels. Our channel hierarchy looks like this:

![csgo-channel-layout](./images/csgo-channel-layout.png)
