# Making Plugins

This section describes how to create a positional audio plugin for a game. To learn about using plugins in the client, go [here](../chapter_4/settings/plugins.html).

## How Do They Work?

Positional audio plugins use fellow players' relative positions in-game to make it sound like their game character is talking to yours. Plugins do this by collecting five data:

* A unique identifier for each player
* Context, which determines whether two players receive positional audio from each other
* In-game position
* The direction the player is facing (orientation/heading)
* Audio

This diagram illustrates how the data flows through each component:

![positional-audio](./images/positional-audio.png)

## Prerequisites

You must install the following software to make a plugin:

* A C/C++ development toolchain
    * GCC or Clang, for example
* CMake
* A C/C++ IDE or text editor
* The plugin framework header files:
    * [MumbleAPI](https://github.com/mumble-voip/mumble/blob/master/plugins/MumbleAPI_v_1_0_x.h)
    * [MumblePlugin](https://github.com/mumble-voip/mumble/blob/master/plugins/MumblePlugin_v_1_0_x.h)
    * [PluginComponents](https://github.com/mumble-voip/mumble/blob/master/plugins/PluginComponents_v_1_0_x.h)

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">The version numbers in the header files change as the API updates.</a>
> </div>

## Setup

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">These instructions assume you're using a UNIX-like platform, such as Linux.</a>
> </div>

Your plugin project needs a directory. Create it by running: `mkdir plugin_name`. Once you've done that, download the three plugin headerfile linked above. Run: `mkdir include && mv *.h include`. Mumble uses
CMake for its build system. Therefore, you need to make a file called `CMakeLists.txt`. This file contains directives that describe the source files and targets for the project. Run: `touch CMakeLists.txt`,
and open it with your text editor or IDE. Paste the following code into it, then save and quit:

```cmake
cmake_minimum_required(VERSION 3.15)

project(MumblePlugin
	VERSION "1.0.0"
	DESCRIPTION "My Mumble plugin"
	LANGUAGES "C"
)

add_library(plugin
	SHARED
		plugin.c
)

target_include_directories(plugin
	PUBLIC "${CMAKE_SOURCE_DIR}/include/"
)
```

This code informs CMake that you want to build a shared library from `plugin.c` and to include all files from the `include` directory. Run: `man cmake-language` to learn more about working with the CMake language.
The directory structure for your project must look like this:

```
.
├── include
│   ├── MumbleAPI_v_1_0_x.h
│   ├── MumblePlugin_v_1_0_x.h
│   └── PluginComponents_v_1_0_x.h
├── CMakeLists.txt
└── plugin.c
```

where `.` is the `plugin_name` directory you made earlier. Finally, run: `touch plugin.c`.

## Writing plugin.c

The main source file for the plugin is `plugin.c`. This code snippet contains the minimum code you need to make a positional audio plugin:

```c
// Include all necessary header files
#include "MumblePlugin_v_1_0_x.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Stores the MumbleAPI struct from the MumbleAPI header.
struct MumbleAPI_v_1_0_x mumbleAPI;
// Sets the plugin's ID
mumble_plugin_id_t ownID;

// The mumble_init function
mumble_error_t mumble_init(mumble_plugin_id_t pluginID) {
	ownID = pluginID;

	if (mumbleAPI.log(ownID, "Hello Mumble") != MUMBLE_STATUS_OK) {
		/* Logging failed. Although this example uses Mumble's logger, 
        it's best practice to log events in your plugin's logging system (if it has one) */
	}

	return MUMBLE_STATUS_OK;
}


void mumble_shutdown() {
	if (mumbleAPI.log(ownID, "Goodbye Mumble") != MUMBLE_STATUS_OK) {
        /* Logging failed. Although this example uses Mumble's logger, 
        it's best practice to log events in your plugin's logging system (if it has one) */
	}
}

// Returns the plugin's name
struct MumbleStringWrapper mumble_getName() {
	static const char *name = "MyPlugin";

	struct MumbleStringWrapper wrapper;
	wrapper.data = name;
	wrapper.size = strlen(name);
	wrapper.needsReleasing = false;

	return wrapper;
}

struct MumbleStringWrapper mumble_getAuthor() {
	static const char *name = "Jane Doe";

	struct MumbleStringWrapper wrapper;
	wrapper.data = name;
	wrapper.size = strlen(name);
	wrapper.needsReleasing = false;

	return wrapper;
}

struct MumbleStringWrapper mumble_getDescription() {
	static const char *name = "My awesome plugin that does awesome things.";

	struct MumbleStringWrapper wrapper;
	wrapper.data = name;
	wrapper.size = strlen(name);
	wrapper.needsReleasing = false;

	return wrapper;
}

mumble_version_t mumble_getAPIVersion() {
	// This constant stores the API version of the included header files
	return MUMBLE_PLUGIN_API_VERSION;
}

// Provides the MumbleAPI functions
void mumble_registerAPIFunctions(void *apiStruct) {
	/* mumble_getAPIVersion returns MUMBLE_PLUGIN_API_VERSION, this cast ensures
	that the passed pointer gets cast to the proper type */
	mumbleAPI = MUMBLE_API_CAST(apiStruct);
}

void mumble_releaseResource(const void *pointer) {
    /* If the MumbleStringWrapper struct variable "needsReleasing" is false, this function never get called.
    If "needsReleasing" is true, Mumble calls this function with a pointer associated to a particular resource
    as the argument. This example assumes that "needsReleasing" is false */
	printf("Called mumble_releaseResource but expected that this never gets called -> Aborting");
	abort();
}

uint32_t mumble_getFeatures() {
    return MUMBLE_FEATURE_POSITIONAL;
}

uint8_t mumble_initPositionalData(const char *const *programNames, const uint64_t *programPIDs, size_t programCount) {
    /* Check whether the supported game is in the list of programs. If yes,
	check whether we can obtain the position data from the game */

	// If everything went well, we return this:
    return MUMBLE_PDEC_OK;
	/* Other return values are:
	MUMBLE_PDEC_ERROR_TEMP -> The plugin can't deliver positional data temporarily
	MUMBLE_PDEC_PERM -> Permanenet error. The plugin can't deliver positional data */
}

bool mumble_fetchPositionalData(float *avatarPos, float *avatarDir, float *avatarAxis, float *cameraPos, float *cameraDir,
                                float *cameraAxis, const char **context, const char **identity) {
    /* Fetch positional data and store it in the respective variables. All fields that can't be filled properly
	must be set to 0 or the empty String "" */

	// If we can obtain positional data
	return true;
	// otherwise return false
}

void mumble_shutdownPositionalData() {
	// Unlink the connection to the supported game
    // Perform potential clean-up code
}
```

The subsection below discusses the Plugin API.

### Plugin API

Mumble has a plugin API that provides C functions for positional audio plugins. The API calls each function from the same execution thread. This table shows each function the API provides with a
corresponding description and whether it's mandatory:

| **Function**                    | **Description**                                                                                                                     | **Mandatory?** |
|-------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------  | -------------- |
| `mumble_init`                   | Registers the ID of the plugin and returns the initialization status                                                                | &check;        |
| `mumble_shutdown`               | Stops the plugin and frees the memory previously allocated to it                                                                    | &check;        |
| `mumble_getName`                | Returns the name of the plugin                                                                                                      | &check;        |
| `mumble_getAPIVersion`          | Returns the version of the Plugin API                                                                                               | &check;        |
| `mumble_registerAPIFunctions`   | Provides the Plugin API with functions that let it interact with the client                                                         | &check;        |
| `mumble_releaseResource`        | Accepts a pointer as an argument and frees the segment of memory associated with it                                                 | &check;        |
| `mumble_setMumbleInfo`          | Returns the client version, the Plugin API version it uses, and the minimum Plugin API version it supports                          | &cross;        | 
| `mumble_getVersion`             | Returns the plugin's version                                                                                                        | &cross;        |
| `mumble_getAuthor`              | Returns the plugin's author                                                                                                         | &cross;        |
| `mumble_getDescription`         | Returns the plugin's description                                                                                                    | &cross;        |
| `mumble_getFeatures`            | Indicates whether a plugin retrieves positional data from a game, modifies audio input and output, both, or none                    | &cross;        |
| `mumble_deactivateFeatures`     | Disables a given feature set. The input options are 1, 10, or 11 for positional data, audio input and output modification, or both  | &cross;        |
| `mumble_initPositionalData`     | Indicates that mumble wants this plugin to request positional audio data. It accepts three arguments: an array of pointers to program names, an array of PIDs corresponding to each program name, and the number of pointers. | &cross; | 

The MumblePlugin header file describes all other functions in detail.

## Getting Positional Audio Data

Positional audio plugins require a triplet of vectors for the in-game character and the camera. Mumble uses a left-hand coordinate system. The figure below shows a visual representation of the left-hand rule:

![left-hand-rule](./images/left-hand-rule.webp)

The three positional audio vectors are:

* The position vector
* The direction vector
* The top vector

The position vector takes values in meters, while the direction and top vectors are [unit vectors](https://en.wikipedia.org/wiki/Unit_vector). The direction vector denotes where the player is facing.
For example, the front vector for a player looking to their right is (1, 0, 0) because the x-axis points right. The top vector points upward. Therefore, its value is always (0, 1, 0). 

You must find the three positional audio vectors. Memory analysis tools help us find the memory addresses corresponding to the in-game positional data. This tutorial covers CheatEngine for Windows and scanmem 
for Linux. 

> <div class="warning box">
>    <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">These tools might may trigger anti-cheat protections in your game.</a>
> </div>

### CheatEngine

Start by finding the position vector. These steps explain finding its three position coordinates in memory:

1. Start the game you're making the plugin for, preferably in windowed mode
2. Start a game session on a map
3. Start Cheat Engine
4. Click on the computer icon below *File*
5. Find your game executable on the list, then double-click it
6. Set *Value type* to `Float` and *Scan type* to `Unknown initial value`
7. Click *First Scan*
8. Move around in-game
9. In CheatEngine, set *Scan type* to `Changed value` and click *Next Scan*
10. Set *Scan type* to `Unchanged value`
11. Wait twenty seconds, then click *Next Scan* six times
12. Repeat steps 8 through 11 three times
13. Go back to the game and look around with your mouse without moving around

Now, Analyze the addresses displayed on the left-hand side of the CheatEngine application window. Look for green-colored addresses. 

To find the front vector, determine what direction north is in the game map. Then, do the following: 

1. In CheatEngine, set *Value type* to `Float` and *Scan type* to `Unknown initial value`, then click *First Scan*
2. Look slightly to the left in-game
3. In CheatEngine, set *Scan type* to `Changed value` and click *Next Scan*
4. Set *Scan type* to `Unchanged value`
5. Wait twenty seconds, then click *Next Scan* six times
6. Move around in-game and repeat steps 3 through 5 three times
7. Look directly north in-game

Because most first-person games have their top vector's memory address within 300 hex of the front vector, you can set a scan range with both endpoints 300 hex from the top vector in CheatEngine. Then, complete
the remaining steps:

1. Go in-game and look straight ahead
2. In CheatEngine, set *Value type* to `Float` and *Scan type* to `Unknown initial value`, then click *First Scan*
3. Look down slightly in-game
4. In CheatEngine, set *Value type* to `Changed value`, then click *Next Scan*
5. Set *Scan type* to `Unchanged value`
6. Wait twenty seconds, then click *Next Scan* six times
7. Look straight forward again

### Scanmem

Most Linux distributions have scanmem in their package repositories. Install it with your package manager. For example, if you're using Debian or Ubuntu, run:

`sudo apt-get install scanmem`

Then, follow these instructions:

1. Launch the game you're making the plugin for
2. Open a terminal, and run `pidof nameofthegamebinary`. If this doesn't work, run `ps -ef | grep nameofgamebinary`
3. In the same terminal, run `sudo scanmem pidofthegame`
4. Move around in-game
