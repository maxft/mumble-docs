# What's a Bot?

A bot is a program that provides a service for users. Bots connect to Mumble servers as clients and exchange information, such as audio and server commands. Bots can execute commands in a few ways:
* When users send messages to the bot using the chat
* When event-based triggers that the programmer implemented in the bot's code occur
* When users send commands using a web interface

# Making a Bot

Libraries such as [pymumble](https://github.com/azlux/pymumble) and [mumble-ruby](https://github.com/mattvperry/mumble-ruby) provide APIs for creating bots.

## Using pymumble

Install pymumble with git or pip as the [README describes](https://github.com/azlux/pymumble#readme). We then start with creating the main program file:

```python
#!/usr/bin/env python

import sys
import os
import os.path
import pymumble_py3 as pymumble

class MumbleBot:
    def __init__(self):
        self.username = "John Doe"
        port = 64738
        password = "youmayenter"
        tokens = ['token1', 'token2']
        self.stereo = True
        self.debug = False
        certfile = "/path/to/certfile.pem"
        self.mumble = pymumble.Mumble(host, user=self.username, port=port, password=password, tokens=tokens,
                                      stereo=self.stereo,
                                      debug=self.debug,
                                      certfile=certificate)

Mumble.start()
```

```python
class Mumble(host, user, port=64738, password='', certfile=None, keyfile=None, reconnect=False, tokens=[], stereo=False, debug=False)
```

This class constructs the Mumble object. The table below describes the Mumble object's parameters:

| Parameter | Type | Description |
| --------- | ---- |----------- |
| `host` | `string` | Mumble server address for the bot to connect to |
| `user` | `string` | Username of the bot | 
| `port` | `integer` | The port of the Mumble server the bot connects to |
| `password` | `string` | The server password. Leave as-is if the server has no password |
| `certfile` | `string` | The path of the bot's client certificate in `.pem` format |
| `keyfile` | `string` | The path of the bot's key file in `.pem` format |
| `reconnect` | `boolean` | Whether the bot reconnects automatically when disconnected from the server |
| `tokens` | `list` | List of channel access tokens |
| `stereo` | `boolean` | Whether to enable stereo audio support |
| `debug` | `boolean` | Whether to generate verbose error messages in stdout |
