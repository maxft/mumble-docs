# What is Mumble?

Mumble is a free and open-source Voice over IP [(VOIP)](https://en.wikipedia.org/wiki/Voice_over_IP) application. It focuses on providing users with low latency and high-quality voice chat.

# Who is Mumble for?

Mumble is for anyone wanting a stable and feature-rich voice chat application. 

## Users

Mumble provides numerous features users appreciate. Some of these include:

* Encrypted voice chat and certificate based authentication for secure communications
* An in-game overlay displaying game FPS and a list of talking users
* A positional audio function that lets users hear their fellow players' audio based on their in-game location
* Simple configuration wizards
* Helpful utilities, such as a built-in audio recorder

Some use cases include:

* Podcast recording with the multi-channel audio recorder
* Gaming, aided by the positional audio feature as well as low resource footprint
* Hosting over 100 simultaneous voice participants
* Hobby radio transmission
* Workplace conferences

## Administrators

Administrators appreciate the flexibility Mumble provides. Mumble uses a client-server model; unlike Discord, 
administrators have complete control over the server application. Administrators can enjoy the following features:

* Self-hostable, allowing for data security and privacy
* Permissive software license, allowing for easier deployment with no limitations
* Open-source, which improves security and allows for modifications
* Extensive permission system with Access Control Lists (ACLs)
* [Ice](https://zeroc.com/products/ice) protocol support
    - You can configure channel viewers and other web interfaces
    - You can authenticate users against your database
    - You can create scripts to automate server administration

# What Platforms Does it Support?

It's cross-platform and available on the following platforms:

* Windows
* MacOS
* Linux
* FreeBSD
* OpenBSD
* NetBSD
* iOS (unmaintained)
* Android (3rd party)
