The following sections inform users how to obtain Mumble by either:

* Downloading a pre-built version
* Building from source

# Quick Start Guide

This guide shows how to configure Mumble enough to connect to a server for the first time on Windows:

## Download

Go [here](https://www.mumble.info/downloads/) and click on your platform:

![manual-download](images/manual-download.png)

> <div class="warning box">
> <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Choose x64 if you don't know what CPU architecture you have.</a>
> </div>

## Run

Open your File Manager, then right click the installer and click "Install":

![file-explorer](images/file-explorer.png)

## Configure

Launch Mumble. The audio and certificate wizards shall open for first-time setup.
The audio wizard launches first:

![audio-wizard](images/audio-wizard.png)

If the speakers and microphone you want to use aren't your system's default, select them from the device drop down menu:

![audio-devices](images/audio-devices.jpg)

If you aren't using push-to-talk, tweak the volume thresholds to ensure Mumble detects your speech:

![volume-thresholds](images/volume-thresholds.png)

When the certificate wizard loads, keep all the defaults. Go to [Chapter 4.1](../chapter_4/authentication.html) to learn more about certificates.

## Connect

The last prompted configuration step is deciding whether you want to see the public server list:

![server-list-consent](images/server-list-consent.png)

To join your server, follow these steps:

1. Click 'Server' in the upper left-hand corner of Mumble's application window and select 'Connect...'. 
2. Click 'Add New...'
3. Enter the server URL or IP address in the address field
4. Change the port number if the administrator informed you it's not the default
5. Enter your username and label the server, then click OK
6. Select the server in your favorites and click 'Connect'
