# Installation

This section instructs users on installing the Mumble client on its supported platforms.

## Windows

Follow these instructions in order:

1. Download the latest stable or development snapshot here
2. Right-click the msi file and click on "Install"
3. Follow the instructions in the installation wizard
4. Run Mumble and follow the Audio and Certificate wizards
5. Enjoy!

> <div class="warning box">
> <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">All platforms use the integrated Audio and Certificate wizards</a>
> </div>

## Mac OS

The Mac OS instructions are practically identical to the Windows instructions, except you're dealing with a dmg file rather than a msi file.

## Linux & BSD

Users have three installation options:

* Your distribution's package manager
* The Snap package
* The Flatpak

### Distribution Package Manager

The terminal command may vary depending on your distribution or BSD variant. Below is a list of installation commands for popular Linux distributions and the BSDs:

**Debian** 

`sudo apt install mumble`

**Ubuntu**

Same as Debian, or run the following commands for the latest version:

```
sudo add-apt-repository ppa:mumble/release
sudo apt update
sudo apt install mumble
```

**Fedora/Red Hat**

`sudo dnf install mumble`

**OpenSUSE**

`sudo zypper install mumble`

**Arch Linux**

`sudo pacman -S mumble`

**Gentoo**

`sudo emerge -av media-sound/mumble`

**FreeBSD**

`sudo pkg install mumble` or `cd /usr/ports/audio/mumble && sudo make install clean`

**OpenBSD**

`doas pkg_add mumble`

**NetBSD**

`sudo pkgin install mumble` or `cd /usr/pkgsrc/chat/mumble && sudo make install clean`

### Snap Package

Snaps are a distro-agnostic packaging standard for Linux. If you want to install it, follow these [instructions](https://snapcraft.io/docs/installing-snapd).

After you configure snapd, run:

`sudo snap install mumble`

### Flatpak

Flatpak is another distro-agnostic packaging standard for Linux. If you want to install it, follow these [instructions](https://flatpak.org/setup/).

After you configure flatpak, run:

`flatpak install flathub info.mumble.Mumble`

## Android

Mumla is a 3rd-party Mumble client for Android. Download it from [F-Droid](https://f-droid.org/packages/se.lublin.mumla/) or [Google Play](https://play.google.com/store/apps/details?id=se.lublin.mumla).

## iOS

You can download it [here](https://apps.apple.com/us/app/mumble/id443472808).

> <div class="warning box">
> <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">The developers haven't maintained it since 2017</a>
> </div>
