# Build Instructions

This section instructs users how to obtain and compile Mumble's source code. Follow these steps to build Mumble on Linux or Mac OS:

## Install Dependencies

The commands vary depending on your Operating System. For Debian/Ubuntu, run:

```bash
sudo apt install \
  build-essential \
  git \
  cmake \
  pkg-config \
  qtbase5-dev \
  qtchooser \
  qttools5-dev \
  qttools5-dev-tools \
  libqt5svg5-dev \
  libboost-dev \
  libssl-dev \
  libprotobuf-dev \
  protobuf-compiler \
  libprotoc-dev \
  libcap-dev \
  libxi-dev \
  libasound2-dev \
  libogg-dev \
  libsndfile1-dev \
  libspeechd-dev \
  libavahi-compat-libdnssd-dev \
  libzeroc-ice-dev \
  libpoco-dev \ 
  g++-multilib \ # Optional if you want overlay support for 32-bit applications on a 64-bit OS
  libgrpc++-dev \ # Optional for gRPC support in Murmur
  protobuf-compiler-grpc # Optional for gRPC support in Murmur
```


> <div class="warning box">
>     <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">You must have CMake version 3.15 or higher and GCC version 5 or higher.</a>
> </div>

For Fedora/Red Hat:

```bash
sudo dnf -y install epel-release # Only for Red Hat
sudo dnf config-manager --set-enabled PowerTools
sudo dnf group install "Development Tools"
sudo dnf install https://zeroc.com/download/ice/3.7/el8/ice-repo-3.7.el8.noarch.rpm
sudo dnf install libice-c++-devel libice3.7-c++
sudo dnf install \
qt5-devel \
qt5-qtsvg-devel  \
openssl-devel \
protobuf-devel \
libsndfile-devel \
libXi-devel \
libXext-devel \
speech-dispatcher-devel \
avahi-compat-libdns_sd-devel \
alsa-lib-devel \
libICE-devel \
libogg-devel \
boost-devel \
libcap-devel \
poco-devel \
gcc-toolset-9-gcc-c++ # Only for Red Hat
```

> <div class="warning box">
>     <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">Remember that the package names likely differ on other Linux distributions.</a>
> </div>

On Mac OS, use [brew](https://brew.sh):

```bash
brew update && brew install \
  cmake \
  git \
  pkg-config \
  qt5 \
  boost \
  libogg \
  libvorbis \
  flac \
  libsndfile \
  protobuf \
  openssl \
  poco \
  ice
```

## Downloading the Source

Run:

```
git clone https://github.com/mumble-voip/mumble.git && cd mumble
git submodule update --init --recursive
```

## Configuring CMake 

Run:

```
mkdir build
cd build
cmake ..
```

### CMake Options

You can use CMake options to customize your Mumble build. The syntax is:

`cmake -D<optionName1>=<value> -D<optionname2>=<value> ..`

For example, `cmake -Dtests=ON ..`

Below is a table describing each build option and their default values:

| **Option** | **Description** | **Default Value** |
| ---------- | -------------- | ----------------- |
| alsa | Build support for ALSA | ON |
| asio | Build support for ASIO audio input | OFF |
| BUILD_OVERLAY_XCOMPILE | Build a 32-bit overlay | OFF |
| bundle-qt-translations | Build QT's translations | ${static} |
| bundled-celt | Use the included version of CELT instead of the system version | ON |
| bundled-opus | Use the included version of Opus instead of the system version | ON |
| bundled-speex | Use the included version of Speex instead of the system version | ON |
| client | Build the Mumble client | ON |
| coreaudio | Support for CoreAudio | ON |
| crash-report | Include support for reporting crashes to the Mumble developers | ON |
| dbus | Support for DBus | ON |
| debug-dependency-search | Prints extended information during the search for the needed dependencies | OFF |
| elevation | Set "uiAccess=true", required for global shortcuts to work with privileged applications. Requires the client's executable to be signed with a trusted code signing certificate | OFF |
| g15 | Include support for the G15 keyboard and other compatible devices | OFF |
| g15-emulator | This will cause an emulated G15 window to appear on screen. Allows the use of Mumble's G15 support without owning the physical hardware | OFF |
| gkey | Support for Logitech G-Keys. Requires you to install the Logitech Gaming Software for any effect at runtime | ON |
| grpc | Support for gRPC | OFF |
| ice | Support for Ice RPC | ON |
| jackaudio | Support for JACK | ON |
| manual-plugin | Include the built-in "manual positional audio plugin" | ON |
| online-tests | Whether to include tests requiring a working internet connection | OFF |
| optimize | Optimize the build for the machine compiling it | OFF |
| OPUS_BUILD_SHARED_LIBRARY | N/A | ON |
| OPUS_STACK_PROTECTOR | N/A | OFF |
| oss | Support for OSS | ON |
| overlay | Build the overlay | ON |
| overlay-xcompile | Build 32-bit overlay library | ON |
| packaging | Build package | OFF |
| pipewire | Support for PipeWire | ON |
| plugin-callback-debug | Build Mumble with debug output for plugin callbacks inside of Mumble | OFF |
| plugin-debug | Build Mumble with debug output for plugin developers | OFF |
| plugins | Build plugins | ON |
| portaudio | Support for PortAudio | ON |
| pulseaudio | Support for PulseAudio | ON |
| qssldiffiehellmanparameters | Support for custom Diffie-Hellman parameters | ON |
| qtspeech | Use QT's text-to-speech system over Mumble's OS-specific text-to-speech implementations | OFF |
| retracted-plugins | Build outdated plugins | OFF |
| rnnoise | Build RNNoise for machine learning noise reduction | ON |
| server | Build the server | ON |
| speechd | Support for Speech Dispatcher | ON |
| static | Build static binaries | OFF |
| VCPKG_TARGET_TRIPLET | The triplet used when installing dependencies with vcpkg | N/A |
| CMAKE_TOOLCHAIN_FILE | Path to the CMake toolchain file | N/A |
| Ice_HOME | Required for static builds when building Ice support | N/A |
| symbols | Build binaries such that debugging is easier | OFF |
| tests | Build tests | OFF |
| translations | Includes languages other than English | ON |
| update | Check for updates by default | ON |
| warnings-as-errors | Treats all warnings as errors | ON |
| wasapi | Support for WASAPI | ON |
| xboxinput | Support for global shortcuts from Xbox controllers via the XInput DLL | ON |
| xinput2 | Support for XI2 | ON |
| zeroconf | Support for zeroconf (mDNS/DNS-SD) | ON |

## Build

The build commands differ depending on platform

### Linux

`make -j $(nproc)`

### MacOS

`cmake --build . -j <jobs>`

where `<jobs>` is the amount of processor threads.

> <div class="warning box">
>     <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">You need to point CMake to the version of OpenSSL you installed with brew by running <tt><font color="red">cmake -DOPENSSL_ROOT_DIR=/
>     path/to/openssl ..</font></tt> when you're setting your other CMake options.</a>
> </div>

## Static Build

Use a static build if you're using Windows or want all the necessary dependencies bundled into the binary. Follow these steps to compile a static binary:

### Installing Dependencies

You'll need these three software packages:

* [vcpkg](https://github.com/Microsoft/vcpkg)
* [CMake](https://cmake.org/download/)
* A C++14 compliant compiler
    * GCC or Clang if you're on Linux or MacOS
    * Microsoft Visual Studio [Build Tools or Visual Studio Community](https://visualstudio.microsoft.com/downloads) if you're on Windows

<h4>With Installation Script</h4>

1. Download the installation script
    * For [Windows](https://github.com/mumble-voip/mumble/blob/master/scripts/vcpkg/get_mumble_dependencies.ps1) it and 
    * For [Linux and MacOS](https://github.com/mumble-voip/mumble/blob/master/scripts/vcpkg/get_mumble_dependencies.sh)
2. Run the script
    * For Windows, open PowerShell, then execute `Set-ExecutionPolicy -Scope CurrentUser RemoteSigned` and run the script
    * For Linux and MacOS, run the script in the terminal

<h4>Manual Installation</h4>

If you don't already have vcpkg, clone the git repository and follow the instructions [here](https://github.com/microsoft/vcpkg#quick-start-windows) for your operating system. Then, follow these steps:

1. Go to the installation directory of vcpkg
2. Install these packages:
```
qt5-base
qt5-svg
qt5-tools
qt5-translations
grpc
boost-accumulators
opus
poco
libvorbis
libogg
libflac
libsndfile
libmariadb
zlib
```
3. If you're on Linux, install these:
    * libxi
    * libgl1-mesa
    * libglu1-mesa
    * mesa-common
    * libxrandr
    * libxxf86vm
    * libbluetooth
    * libx11-xcb
    * python3

> <div class="warning box">
>     <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">MacOS needs Xquartz.</a>
> </div>

> <div class="warning box">
>     <i class="fa fa-exclamation-circle fa-2x"></i><a style="margin-left: 10px">If you're building Murmur and want Ice RPC support, download and copy this
>     <a href="https://github.com/mumble-voip/mumble/blob/master/helpers/vcpkg/ports/zeroc-ice">directory</a> <a>to <tt><font color="red">vcpkgdir/ports/</font></tt>, where vcpkgdir is the installation directory of 
>     vcpkg.</a>
> </div>

### Building

Now that you installed all required dependencies, follow these steps:

1. Open a terminal ("Developer Command Prompt" on Windows)
2. Git clone https://github.com/mumble-voip/mumble.git
3. Go to the cloned directory
4. Create a directory named `build` and go to it
5. Run the CMake generator
    * Linux: `cmake "-DVCPKG_TARGET_TRIPLET=x64-linux" "-Dstatic=ON" "-DCMAKE_TOOLCHAIN_FILE=<vcpkg dir>/scripts/buildsystems/vcpkg.cmake" "-DIce_HOME=<vcpkg dir>/installed/x64-linux" "-DCMAKE_BUILD_TYPE=Release" ..`
    * MacOS: `cmake "-DVCPKG_TARGET_TRIPLET=x64-osx" "-Dstatic=ON" "-DCMAKE_TOOLCHAIN_FILE=<vcpkg dir>/scripts/buildsystems/vcpkg.cmake" "-DIce_HOME=<vcpkg dir>/installed/x64-osx" "-DCMAKE_BUILD_TYPE=Release" ..`
    * Windows: `cmake -G "NMake Makefiles" "-DVCPKG_TARGET_TRIPLET=x64-windows-static-md" "-Dstatic=ON" "-DCMAKE_TOOLCHAIN_FILE=<vcpkg_root>/scripts/buildsystems/vcpkg.cmake" "-DIce_HOME=<vcpkg_root>/installed/x64-windows-static-md" "-DCMAKE_BUILD_TYPE=Release" ..`
6. Run CMake build mode with `cmake --build .`
